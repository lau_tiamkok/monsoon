<?php
// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/settings', function (Request $request, Response $response, $args) {

    // If you want to access the Slim settings again.
    // $settings = $this->get("settings");

    // Log anything.
    // $this->get('Monolog\Logger')->addInfo("Logged from route /settings");

    // Autowiring the controller.
    $controller = $this->get('Spectre\Setting\Controller\Fetch\Settings');

    // Obtain result.
    $settings = $controller->fetch($request);
    $response->getBody()->write(json_encode($settings));
});
