<?php
namespace Spectre\Setting\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Setting\Controller\Controller;
use \Spectre\Setting\Gateway\Fetch\Setting as FetchGateway;
use \Spectre\Setting\Mapper\Fetch\Setting as FetchMapper;

class SettingByTypeSlug extends Controller
{
    public function fetch(Request $request, array $args)
    {
        $gateway = new FetchGateway($this->database);
        $mapper = new FetchMapper($gateway);
        $model = $mapper->fetch([
            'uuid',
            'slug',
            'value',
            'created_on',
            'updated_on',
        ], [
            'type' => $args['type'],
            'slug' => $args['slug'],
        ]);

        $model->setCreatedOn($this->timestamp->convert($model->getCreatedOn()));
        $model->setUpdatedOn($this->timestamp->convert($model->getUpdatedOn()));
        return $model->toArray();
    }
}
