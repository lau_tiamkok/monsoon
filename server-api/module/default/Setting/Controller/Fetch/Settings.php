<?php
namespace Spectre\Setting\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Setting\Controller\Controller;
use \Spectre\Setting\Gateway\Fetch\Settings as FetchGateway;
use \Spectre\Setting\Mapper\Fetch\Settings as FetchMapper;

class Settings extends Controller
{
    public function fetch(Request $request)
    {
        $gateway = new FetchGateway($this->database);
        $mapper = new FetchMapper($gateway);
        $collection = $mapper->fetch([
            'uuid',
            'slug',
            'type',
            'value',
            'created_on',
            'updated_on',
        ]);

        $settings = $collection->getItems();
        foreach ($settings as $key => $setting) {
            $setting->setCreatedOn($this->timestamp->convert($setting->getCreatedOn()));
            $setting->setUpdatedOn($this->timestamp->convert($setting->getUpdatedOn()));
        }

        return $collection->toArray();
    }
}
