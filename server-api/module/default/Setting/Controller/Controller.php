<?php
namespace Spectre\Setting\Controller;

// Vendors.
use \Medoo\Medoo;
use \Monolog\Logger;

// Cores.
use \Spectre\Core\Controller\Controller as CoreController;
use \Spectre\Core\Utils\TimestampConvertor;

abstract class Controller extends CoreController
{
    protected $database;
    protected $timestamp;

    public function __construct(
        Medoo $database,
        TimestampConvertor $timestamp,
        Logger $logger
    ) {
        $this->database = $database;
        $this->timestamp = $timestamp;

        // Log anything.
        $logger->addInfo("Logged from Spectre\Setting\Controller\Controller");
    }
}
