<?php
namespace Spectre\Setting\Controller\Update;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Setting\Controller\Controller;
use \Spectre\Setting\Gateway\Update\Setting as UpdateGateway;
use \Spectre\Setting\Mapper\Update\Setting as UpdateMapper;

class Setting extends Controller
{
    public function update(Request $request)
    {
        // Get params and validate them here.
        $uuid = $request->getParam('uuid');
        $type = $request->getParam('type');
        $slug = $request->getParam('slug');
        $value = $request->getParam('value');

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Throw if empty.
        if (!$type) {
            throw new \Exception('$type is empty', 400);
        }

        // Throw if empty.
        if (!$slug) {
            throw new \Exception('$slug is empty', 400);
        }

        // Throw if empty.
        if (!$value) {
            throw new \Exception('$value is empty', 400);
        }

        // Create a timestamp.
        $date = new \DateTime();
        $timestamp = $date->getTimestamp();

        $gateway = new UpdateGateway($this->database);
        $mapper = new UpdateMapper($gateway);
        $model = $mapper->update([
            "uuid" => $uuid,
            "type" => $type,
            "slug" => $slug,
            "value" => $value,
            'updated_on' => $timestamp,
        ], [
            "uuid" => $uuid
        ]);

        return $model->toArray();
    }
}
