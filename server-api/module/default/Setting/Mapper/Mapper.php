<?php
namespace Spectre\Setting\Mapper;

use \Spectre\Core\Strategy\GatewayInterface;
use \Spectre\Core\Mapper\Mapper as CoreMapper;

abstract class Mapper extends CoreMapper
{
    protected $gateway;
    protected $model = 'Spectre\Setting\Model\Model';
    protected $collection = 'Spectre\Setting\Model\Collection';

    public function __construct(GatewayInterface $gateway)
    {
        $this->gateway = $gateway;
    }
}
