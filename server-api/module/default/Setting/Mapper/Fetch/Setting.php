<?php
namespace Spectre\Setting\Mapper\Fetch;

use \Spectre\Setting\Mapper\Mapper;

class Setting extends Mapper
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($where) === 0) {
            throw new \Exception('$where is empty from the controller', 400);
        }

        $setting = $this->gateway->fetch($columns, $where);

        // Throw error if no setting found.
        if ($setting === null) {
            throw new \Exception('No setting found.', 400);
        }
        return $this->mapObject($setting);
    }
}
