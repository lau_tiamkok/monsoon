<?php
// require '_routes/fetch_settings.php';
// require '_routes/fetch_setting.php';
// require '_routes/insert_setting.php';
// require '_routes/update_setting.php';
// require '_routes/delete_setting.php';

// Get location of current file.
$path = __DIR__ . '/_routes/';

// Scan and list the routes.
$routes = scandir($path);
$routes = array_diff(scandir($path), array('.', '..'));
foreach ($routes as $route) {
    require $path . $route;
}
