<?php
namespace Spectre\Setting\Gateway\Update;

use \Spectre\Core\Model\Model;
use \Spectre\Setting\Gateway\Gateway;

class Setting extends Gateway
{
    public function update(Model $setting, array $where)
    {
        // Get data from the model.
        $type = $setting->getType();
        $slug = $setting->getSlug();
        $value = $setting->getValue();
        $updatedOn = $setting->getUpdatedOn();

        // Get data from the params.
        $uuid = $where['uuid'];

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Throw if empty.
        if (!$type) {
            throw new \Exception('$type is empty', 400);
        }

        // Throw if empty.
        if (!$slug) {
            throw new \Exception('$slug is empty', 400);
        }

        // Throw if empty.
        if (!$value) {
            throw new \Exception('$value is empty', 400);
        }

        // Throw if empty.
        if (!$updatedOn) {
            throw new \Exception('$updatedOn is empty', 400);
        }

        // Update setting(s).
        // https://medoo.in/api/update
        $result = $this->database->update("setting", [
            "type" => $type,
            "slug" => $slug,
            "value" => $value,
            "updated_on" => $updatedOn
        ], [
            "uuid" => $uuid
        ]);

        // Throw if it fails.
        // Returns the number of rows affected by the last SQL statement.
        if ($result->rowCount() === 0) {
            throw new \Exception('Update row failed', 400);
        }

        // print_r($this->database->error());

        // Return the model if it is OK.
        return $setting;
    }
}
