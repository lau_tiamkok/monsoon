<?php
namespace Spectre\Setting\Gateway\Insert;

use \Spectre\Core\Model\Model;
use \Spectre\Setting\Gateway\Gateway;

class Setting extends Gateway
{
    public function insert(Model $setting)
    {
        // Get data from the model.
        $uuid = $setting->getUuid();
        $type = $setting->getType();
        $slug = $setting->getSlug();
        $value = $setting->getValue();
        $createdOn = $setting->getCreatedOn();

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Throw if empty.
        if (!$type) {
            throw new \Exception('$type is empty', 400);
        }

        // Throw if empty.
        if (!$slug) {
            throw new \Exception('$slug is empty', 400);
        }

        // Throw if empty.
        if (!$slug) {
            throw new \Exception('$slug is empty', 400);
        }

        // Throw if empty.
        if (!$value) {
            throw new \Exception('$value is empty', 400);
        }

        // Throw if empty.
        if (!$createdOn) {
            throw new \Exception('$createdOn is empty', 400);
        }

        // Insert setting.
        // https://medoo.in/api/insert
        $result = $this->database->insert('setting', [
            'uuid' => $uuid,
            'type' => $type,
            'slug' => $slug,
            'value' => $value,
            'created_on' => $createdOn
        ]);

        // Throw if it fails.
        // Returns the number of rows affected by the last SQL statement.
        if ($result->rowCount() === 0) {
            throw new \Exception('Insert row failed', 400);
        }

        // Return the model if it is OK.
        return $setting;
    }
}
