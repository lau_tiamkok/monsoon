<?php
namespace Spectre\Setting\Gateway\Delete;

use \Spectre\Setting\Gateway\Gateway;
use \Spectre\Setting\Model\Model;

class Setting extends Gateway
{
    public function delete(Model $setting)
    {
        // Get data from the model.
        $uuid = $setting->getUuid();

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Delete setting(s).
        // https://medoo.in/api/delete
        $result = $this->database->delete("setting", [
            "uuid" => $uuid
        ]);

        // Check the number of rows affected by the last SQL statement.
        // Throw if it fails.
        if ($result->rowCount() === 0) {
            throw new \Exception('Delete row failed', 400);
        }

        // Return the model if it is OK.
        return $setting;
    }
}
