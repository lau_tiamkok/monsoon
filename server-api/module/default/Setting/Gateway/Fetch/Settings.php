<?php
namespace Spectre\Setting\Gateway\Fetch;

use \Spectre\Setting\Gateway\Gateway;

class Settings extends Gateway
{
    public function fetch($columns = [])
    {
        // Get setting(s).
        // https://medoo.in/api/select
        $data = $this->database->select('setting', $columns);

        // Return the result.
        return $data;
    }
}
