<?php
namespace Spectre\Setting\Gateway\Fetch;

use \Spectre\Setting\Gateway\Gateway;

class Setting extends Gateway
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($where) === 0) {
            throw new \Exception('$where is empty from the mapper', 400);
        }

        // Get setting.
        // https://medoo.in/api/get
        $data = $this->database->get('setting', $columns, $where);

        // Throw error if no result found.
        if ($data === false) {
            throw new \Exception('No setting found', 400);
        }

        // Return the result.
        return $data;
    }
}
