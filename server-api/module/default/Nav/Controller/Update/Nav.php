<?php
namespace Spectre\Nav\Controller\Update;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Nav\Controller\Controller;
use \Spectre\Nav\Gateway\Update\Nav as UpdateGateway;
use \Spectre\Nav\Mapper\Update\Nav as UpdateMapper;

class Nav extends Controller
{
    public function update(Request $request)
    {
        // Get params and validate them here.
        $uuid = $request->getParam('uuid');
        $title = $request->getParam('title');
        $slug = $request->getParam('slug');

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Throw if empty.
        if (!$title) {
            throw new \Exception('$title is empty', 400);
        }

        // Throw if empty.
        if (!$slug) {
            throw new \Exception('$slug is empty', 400);
        }

        // Create a timestamp.
        $date = new \DateTime();
        $timestamp = $date->getTimestamp();

        $gateway = new UpdateGateway($this->database);
        $mapper = new UpdateMapper($gateway);
        $model = $mapper->update([
            'uuid' => $uuid,
            'title' => $title,
            'slug' => $slug,
            'updated_on' => $timestamp,
        ], [
            'uuid' => $uuid
        ]);

        return $model->toArray();
    }
}
