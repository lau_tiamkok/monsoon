<?php
namespace Spectre\Nav\Controller\Insert;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// Uuid generator.
use \Ramsey\Uuid\Uuid;

use \Spectre\Nav\Controller\Controller;
use \Spectre\Nav\Gateway\Insert\Nav as InsertGateway;
use \Spectre\Nav\Mapper\Insert\Nav as InsertMapper;

class Nav extends Controller
{
    public function insert(Request $request)
    {
        // Get params and validate them here.
        // POST or PUT
        // $allPostPutVars = $request->getParsedBody();
        // foreach($allPostPutVars as $key => $param){
        //     //POST or PUT parameters list
        //     var_dump($param);
        // }
        // // Single POST/PUT parameter
        // $name = $allPostPutVars['name'];
        // $email = $allPostPutVars['email'];

        // Or, better:
        $title = $request->getParam('title');
        $slug = $request->getParam('slug');
        $articleUuid = $request->getParam('article_uuid');

        // Throw if empty.
        if (!$title) {
            throw new \Exception('$title is empty', 400);
        }

        // Throw if empty.
        if (!$slug) {
            throw new \Exception('$slug is empty', 400);
        }

        // Create a timestamp.
        $date = new \DateTime();
        $timestamp = $date->getTimestamp();
        // Or:
        // $timestamp = time();

        // Generate a version 3 (name-based and hashed with MD5) UUID object.
        // https://github.com/ramsey/uuid
        $uuid3 = Uuid::uuid3(Uuid::NAMESPACE_DNS, $title . $timestamp);
        $uuid = $uuid3->toString();

        $gateway = new InsertGateway($this->database);
        $mapper = new InsertMapper($gateway);
        $model = $mapper->insert([
            'uuid' => $uuid,
            'article_uuid' => $articleUuid,
            'title' => $title,
            'slug' => $slug,
            'created_on' => $timestamp,

            // Below shows that you won't be able to set this in the model as it
            // is not declared in the model itself.
            'id' => 123
        ]);

        return $model->toArray();
    }
}
