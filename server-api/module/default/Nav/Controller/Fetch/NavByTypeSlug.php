<?php
namespace Spectre\Nav\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Nav\Controller\Controller;
use \Spectre\Nav\Gateway\Fetch\Nav as FetchGateway;
use \Spectre\Nav\Mapper\Fetch\Nav as FetchMapper;

class NavByTypeSlug extends Controller
{
    public function fetch(Request $request, array $args)
    {
        $gateway = new FetchGateway($this->database);
        $mapper = new FetchMapper($gateway);
        $model = $mapper->fetch([
            'uuid',
            'slug',
            'title',
            'created_on',
            'updated_on',
        ], [
            'type' => $args['type'],
            'slug' => $args['slug'],
        ]);

        $model->setCreatedOn($this->timestamp->convert($model->getCreatedOn()));
        $model->setUpdatedOn($this->timestamp->convert($model->getUpdatedOn()));
        return $model->toArray();
    }
}
