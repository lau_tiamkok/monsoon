<?php
namespace Spectre\Nav\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Nav\Controller\Controller;
use \Spectre\Nav\Gateway\Fetch\Navs as FetchGateway;
use \Spectre\Nav\Mapper\Fetch\Navs as FetchMapper;

class Navs extends Controller
{
    public function fetch(Request $request)
    {
        $gateway = new FetchGateway($this->database);
        $mapper = new FetchMapper($gateway);
        $collection = $mapper->fetch([
            'uuid',
            'slug',
            'title',
            'created_on',
            'updated_on',
        ]);

        $navs = $collection->getItems();
        foreach ($navs as $key => $nav) {
            $nav->setCreatedOn($this->timestamp->convert($nav->getCreatedOn()));
            $nav->setUpdatedOn($this->timestamp->convert($nav->getUpdatedOn()));
        }

        return $collection->toArray();
    }
}
