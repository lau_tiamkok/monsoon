<?php
// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/navs', function (Request $request, Response $response, $args) {

    // If you want to access the Slim settings again.
    // $settings = $this->get("settings");

    // Log anything.
    // $this->get('Monolog\Logger')->addInfo("Logged from route /navs");

    // Autowiring the controller.
    $controller = $this->get('Spectre\Nav\Controller\Fetch\Navs');

    // Obtain result.
    $navs = $controller->fetch($request);
    $response->getBody()->write(json_encode($navs));
});
