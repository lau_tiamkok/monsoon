<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/navs/{type:[a-z-\-]+}/{slug:[a-z0-9-\-]+}', function (Request $request, Response $response, $args) {

    // Autowiring the controller.
    $controller = $this->get('Spectre\Nav\Controller\Fetch\NavByTypeSlug');

    // Obtain result.
    $nav = $controller->fetch($request, $args);
    $response->getBody()->write(json_encode($nav));
});
