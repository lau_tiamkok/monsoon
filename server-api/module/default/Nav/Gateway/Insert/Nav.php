<?php
namespace Spectre\Nav\Gateway\Insert;

use \Spectre\Core\Model\Model;
use \Spectre\Nav\Gateway\Gateway;

class Nav extends Gateway
{
    public function insert(Model $nav)
    {
        // Get data from the model.
        $uuid = $nav->getUuid();
        $title = $nav->getTitle();
        $type = $nav->getType();
        $slug = $nav->getSlug();
        $createdOn = $nav->getCreatedOn();

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Throw if empty.
        if (!$title) {
            throw new \Exception('$title is empty', 400);
        }

        // Throw if empty.
        if (!$slug) {
            throw new \Exception('$slug is empty', 400);
        }

        // Throw if empty.
        if (!$type) {
            throw new \Exception('$type is empty', 400);
        }

        // Throw if empty.
        if (!$createdOn) {
            throw new \Exception('$createdOn is empty', 400);
        }

        // Insert nav.
        // https://medoo.in/api/insert
        $result = $this->database->insert('nav', [
            'uuid' => $uuid,
            'slug' => $slug,
            'title' => $title,
            'type' => $type,
            'created_on' => $createdOn,
            'updated_on' => $createdOn,
        ]);

        // Throw if it fails.
        // Returns the number of rows affected by the last SQL statement.
        if ($result->rowCount() === 0) {
            throw new \Exception('Insert row failed', 400);
        }

        // Return the model if it is OK.
        return $nav;
    }
}
