<?php
namespace Spectre\Nav\Gateway\Fetch;

use \Spectre\Nav\Gateway\Gateway;

class Navs extends Gateway
{
    public function fetch($columns = [])
    {
        // Get nav(s).
        // https://medoo.in/api/select
        $data = $this->database->select('nav', $columns);

        // Return the result.
        return $data;
    }
}
