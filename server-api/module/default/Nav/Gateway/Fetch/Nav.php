<?php
namespace Spectre\Nav\Gateway\Fetch;

use \Spectre\Nav\Gateway\Gateway;

class Nav extends Gateway
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($where) === 0) {
            throw new \Exception('$where is empty from the mapper', 400);
        }

        // Get nav.
        // https://medoo.in/api/get
        $data = $this->database->get('nav', $columns, $where);

        // Throw error if no result found.
        if ($data === false) {
            throw new \Exception('No nav found', 400);
        }

        // Return the result.
        return $data;
    }
}
