<?php
namespace Spectre\Nav\Gateway\Delete;

use \Spectre\Nav\Gateway\Gateway;
use \Spectre\Nav\Model\Model;

class Nav extends Gateway
{
    public function delete(Model $nav)
    {
        // Get data from the model.
        $uuid = $nav->getUuid();

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Delete nav(s).
        // https://medoo.in/api/delete
        $result = $this->database->delete("nav", [
            "uuid" => $uuid
        ]);

        // Check the number of rows affected by the last SQL statement.
        // Throw if it fails.
        if ($result->rowCount() === 0) {
            throw new \Exception('Delete row failed', 400);
        }

        // Return the model if it is OK.
        return $nav;
    }
}
