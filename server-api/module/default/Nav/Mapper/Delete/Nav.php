<?php
namespace Spectre\Nav\Mapper\Delete;

use \Spectre\Nav\Mapper\Mapper;

class Nav extends Mapper
{
    public function delete($data = [])
    {
        // Throw error if array is empty.
        if (count($data) === 0) {
            throw new \Exception('$data is empty from the controller', 400);
        }

        $model = $this->mapObject($data);
        return $this->gateway->delete($model);
    }
}
