<?php
namespace Spectre\Nav\Mapper;

use \Spectre\Core\Strategy\GatewayInterface;
use \Spectre\Core\Mapper\Mapper as CoreMapper;

abstract class Mapper extends CoreMapper
{
    protected $gateway;
    protected $model = 'Spectre\Nav\Model\Model';
    protected $collection = 'Spectre\Nav\Model\Collection';

    public function __construct(GatewayInterface $gateway)
    {
        $this->gateway = $gateway;
    }
}
