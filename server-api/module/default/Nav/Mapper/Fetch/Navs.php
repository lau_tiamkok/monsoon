<?php
namespace Spectre\Nav\Mapper\Fetch;

use \Spectre\Nav\Mapper\Mapper;

class Navs extends Mapper
{
    public function fetch($columns = [])
    {
        // Throw error if where search is not provide.
        if (count($columns) === 0) {
            throw new \Exception('$columns is empty from the controller', 400);
        }

        $collection = $this->gateway->fetch($columns);
        return $this->mapCollection($collection);
    }
}
