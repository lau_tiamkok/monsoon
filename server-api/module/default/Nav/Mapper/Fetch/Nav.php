<?php
namespace Spectre\Nav\Mapper\Fetch;

use \Spectre\Nav\Mapper\Mapper;

class Nav extends Mapper
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($where) === 0) {
            throw new \Exception('$where is empty from the controller', 400);
        }

        $nav = $this->gateway->fetch($columns, $where);

        // Throw error if no nav found.
        if ($nav === null) {
            throw new \Exception('No nav found.', 400);
        }
        return $this->mapObject($nav);
    }
}
