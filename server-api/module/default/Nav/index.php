<?php
// require '_routes/fetch_navs.php';
// require '_routes/fetch_nav.php';
// require '_routes/insert_nav.php';
// require '_routes/update_nav.php';
// require '_routes/delete_nav.php';

// Get location of current file.
$path = __DIR__ . '/_routes/';

// Scan and list the routes.
$routes = scandir($path);
$routes = array_diff(scandir($path), array('.', '..'));
foreach ($routes as $route) {
    require $path . $route;
}
