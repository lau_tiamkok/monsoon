<?php
namespace Spectre\Nav\Model;

use \Spectre\Core\Model\Model as AbstractModel;

class Model extends AbstractModel
{
    protected $uuid;
    protected $slug;
    protected $type;
    protected $title;
    protected $created_on;
    protected $updated_on;

    /**
     * [__construct description]
     * @param array $params [description]
     */
    public function __construct(array $params = [])
    {
        $this->setOptions($params);
    }

    /**
     * [setOptions description]
     * @param array $params [description]
     */
    public function setOptions(array $params)
    {
        foreach ($params as $key => $value) {
            switch ($key) {
                case 'uuid':
                    $this->setUuid($value);
                    break;
                case 'slug':
                    $this->setSlug($value);
                    break;
                case 'type':
                    $this->setType($value);
                    break;
                case 'title':
                    $this->setTitle($value);
                    break;
                case 'created_on':
                    $this->setCreatedOn($value);
                    break;
                case 'updated_on':
                    $this->setUpdatedOn($value);
                    break;
            }
        }
    }

    // Setters:

    /**
     * [setUuid description]
     * @param [type] $uuid [description]
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * [setTitle description]
     * @param [type] $title [description]
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * [setTitle description]
     * @param [type] $title [description]
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * [setType description]
     * @param [type] $type [description]
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * [setCreatedOn description]
     * @param [type] $createdOn [description]
     */
    public function setCreatedOn($createdOn)
    {
        $this->created_on = $createdOn;
        return $this;
    }

    /**
     * [setUpdatedOn description]
     * @param [type] $updatedOn [description]
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updated_on = $updatedOn;
        return $this;
    }

    // Getters:

    /**
     * [getUuid description]
     * @return [type] [description]
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * [getTitle description]
     * @return [type] [description]
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * [getTitle description]
     * @return [type] [description]
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * [getSlug description]
     * @return [type] [description]
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * [getCreatedOn description]
     * @return [type] [description]
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * [getUpdatedOn description]
     * @return [type] [description]
     */
    public function getUpdatedOn()
    {
        return $this->updated_on;
    }
}
