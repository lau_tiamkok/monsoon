<?php
namespace Spectre\User\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\User\Controller\Controller;
use \Spectre\User\Gateway\Fetch\User as FetchGateway;
use \Spectre\User\Mapper\Fetch\User as FetchMapper;

class UserByTypeSlug extends Controller
{
    public function fetch(Request $request, array $args)
    {
        $gateway = new FetchGateway($this->database);
        $mapper = new FetchMapper($gateway);
        $model = $mapper->fetch([
            'uuid',
            'slug',
            'email',
            'created_on',
            'updated_on',
        ], [
            'type' => $args['type'],
            'slug' => $args['slug'],
        ]);

        $model->setCreatedOn($this->timestamp->convert($model->getCreatedOn()));
        $model->setUpdatedOn($this->timestamp->convert($model->getUpdatedOn()));
        return $model->toArray();
    }
}
