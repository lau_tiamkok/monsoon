<?php
// require '_routes/fetch_users.php';
// require '_routes/fetch_user.php';
// require '_routes/insert_user.php';
// require '_routes/update_user.php';
// require '_routes/delete_user.php';

// Get location of current file.
$path = __DIR__ . '/_routes/';

// Scan and list the routes.
$routes = scandir($path);
$routes = array_diff(scandir($path), array('.', '..'));
foreach ($routes as $route) {
    require $path . $route;
}
