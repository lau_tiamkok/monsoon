<?php
namespace Spectre\User\Gateway\Update;

use \Spectre\Core\Model\Model;
use \Spectre\User\Gateway\Gateway;

class User extends Gateway
{
    public function update(Model $user, array $where)
    {
        // Get data from the model.
        $slug = $user->getSlug();
        $email = $user->getEmail();
        $updatedOn = $user->getUpdatedOn();

        // Get data from the params.
        $uuid = $where['uuid'];

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Throw if empty.
        if (!$slug) {
            throw new \Exception('$slug is empty', 400);
        }

        // Throw if empty.
        if (!$email) {
            throw new \Exception('$email is empty', 400);
        }

        // Throw if empty.
        if (!$updatedOn) {
            throw new \Exception('$updatedOn is empty', 400);
        }

        // Update user(s).
        // https://medoo.in/api/update
        $result = $this->database->update('user', [
            'slug' => $slug,
            'email' => $email,
            'updated_on' => $updatedOn
        ], [
            'uuid' => $uuid
        ]);

        // Throw if it fails.
        // Returns the number of rows affected by the last SQL statement.
        if ($result->rowCount() === 0) {
            throw new \Exception('Update row failed', 400);
        }

        // print_r($this->database->error());

        // Return the model if it is OK.
        return $user;
    }
}
