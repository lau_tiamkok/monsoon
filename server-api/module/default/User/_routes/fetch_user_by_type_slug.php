<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/users/{type:[a-z0-9-\-]+}/{slug}', function (Request $request, Response $response, $args) {

    // Autowiring the controller.
    $controller = $this->get('Spectre\User\Controller\Fetch\UserByTypeSlug');

    // Obtain result.
    $user = $controller->fetch($request, $args);
    $response->getBody()->write(json_encode($user));
});
