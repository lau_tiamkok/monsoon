<?php
namespace Spectre\User\Model;

use \Spectre\Core\Model\Model as AbstractModel;

class Model extends AbstractModel
{
    protected $uuid;
    protected $email;
    protected $slug;
    protected $created_on;
    protected $updated_on;

    /**
     * [__construct description]
     * @param array $params [description]
     */
    public function __construct(array $params = [])
    {
        $this->setOptions($params);
    }

    /**
     * [setOptions description]
     * @param array $params [description]
     */
    public function setOptions(array $params)
    {
        foreach ($params as $key => $value) {
            switch ($key) {
                case 'uuid':
                    $this->setUuid($value);
                    break;
                case 'email':
                    $this->setEmail($value);
                    break;
                case 'slug':
                    $this->setSlug($value);
                    break;
                case 'created_on':
                    $this->setCreatedOn($value);
                    break;
                case 'updated_on':
                    $this->setUpdatedOn($value);
                    break;
            }
        }
    }

    // Setters:

    /**
     * [setUuid description]
     * @param [type] $uuid [description]
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * [setslug description]
     * @param [type] $slug [description]
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * [setslug description]
     * @param [type] $slug [description]
     */
    public function setslug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * [setCreatedOn description]
     * @param [type] $createdOn [description]
     */
    public function setCreatedOn($createdOn)
    {
        $this->created_on = $createdOn;
        return $this;
    }

    /**
     * [setUpdatedOn description]
     * @param [type] $updatedOn [description]
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updated_on = $updatedOn;
        return $this;
    }

    // Getters:

    /**
     * [getUuid description]
     * @return [type] [description]
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * [getslug description]
     * @return [type] [description]
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * [getslug description]
     * @return [type] [description]
     */
    public function getslug()
    {
        return $this->slug;
    }

    /**
     * [getCreatedOn description]
     * @return [type] [description]
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * [getUpdatedOn description]
     * @return [type] [description]
     */
    public function getUpdatedOn()
    {
        return $this->updated_on;
    }
}
