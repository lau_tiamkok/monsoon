<?php
namespace Spectre\Template\Mapper\Fetch;

use \Spectre\Template\Mapper\Mapper;

class Template extends Mapper
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($where) === 0) {
            throw new \Exception('$where is empty from the controller', 400);
        }

        $template = $this->gateway->fetch($columns, $where);

        // Throw error if no Template found.
        if ($template === null) {
            throw new \Exception('No template found.', 400);
        }
        return $this->mapObject($template);
    }
}
