<?php
namespace Spectre\Template\Mapper;

use \Spectre\Core\Strategy\GatewayInterface;
use \Spectre\Core\Mapper\Mapper as CoreMapper;

abstract class Mapper extends CoreMapper
{
    protected $gateway;
    protected $model = 'Spectre\Template\Model\Template';
    protected $collection = 'Spectre\Template\Model\Templates';

    public function __construct(GatewayInterface $gateway)
    {
        $this->gateway = $gateway;
    }
}
