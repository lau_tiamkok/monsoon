<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/templates/{slug:[a-z0-9-\-]+}', function (Request $request, Response $response, $args) {

    // Autowiring the controller.
    $controller = $this->get('Spectre\Template\Controller\Fetch\TemplateBySlug');

    // Obtain result.
    $nav = $controller->fetch($request, $args);
    $response->getBody()->write(json_encode($nav));
});
