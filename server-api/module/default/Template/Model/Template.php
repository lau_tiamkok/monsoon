<?php
namespace Spectre\Template\Model;

use \Spectre\Core\Model\Model as AbstractModel;

class Template extends AbstractModel
{
    protected $uuid;
    protected $slug;
    protected $title;
    protected $filename;
    protected $created_on;
    protected $updated_on;

    /**
     * [__construct description]
     * @param array $params [description]
     */
    public function __construct(array $params = [])
    {
        $this->setOptions($params);
    }

    /**
     * [setOptions description]
     * @param array $params [description]
     */
    public function setOptions(array $params)
    {
        foreach ($params as $key => $value) {
            switch ($key) {
                case 'uuid':
                    $this->setUuid($value);
                    break;
                case 'slug':
                    $this->setSlug($value);
                    break;
                case 'title':
                    $this->setTitle($value);
                    break;
                case 'filename':
                    $this->setFilename($value);
                    break;
                case 'created_on':
                    $this->setCreatedOn($value);
                    break;
                case 'updated_on':
                    $this->setUpdatedOn($value);
                    break;
            }
        }
    }

    // Setters:

    /**
     * [setUuid description]
     * @param [type] $uuid [description]
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * [setTitle description]
     * @param [type] $title [description]
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * [setTitle description]
     * @param [type] $title [description]
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * [setFilename description]
     * @param [type] $filename [description]
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * [setCreatedOn description]
     * @param [type] $createdOn [description]
     */
    public function setCreatedOn($createdOn)
    {
        $this->created_on = $createdOn;
        return $this;
    }

    /**
     * [setUpdatedOn description]
     * @param [type] $updatedOn [description]
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updated_on = $updatedOn;
        return $this;
    }

    // Getters:

    /**
     * [getUuid description]
     * @return [type] [description]
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * [getTitle description]
     * @return [type] [description]
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * [getTitle description]
     * @return [type] [description]
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * [getFilename description]
     * @return [type] [description]
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * [getCreatedOn description]
     * @return [type] [description]
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * [getUpdatedOn description]
     * @return [type] [description]
     */
    public function getUpdatedOn()
    {
        return $this->updated_on;
    }
}
