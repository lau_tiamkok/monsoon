<?php
namespace Spectre\Template\Gateway\Update;

use \Spectre\Core\Model\Model;
use \Spectre\Template\Gateway\Gateway;

class Template extends Gateway
{
    public function update(Model $template, array $where)
    {
        // Get data from the model.
        $title = $template->getTitle();
        $slug = $template->getslug();
        $filename = $template->getFilename();
        $updatedOn = $template->getUpdatedOn();

        // Get data from the params.
        $uuid = $where['uuid'];

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Throw if empty.
        if (!$title) {
            throw new \Exception('$title is empty', 400);
        }

        // Throw if empty.
        if (!$slug) {
            throw new \Exception('$slug is empty', 400);
        }

        // Throw if empty.
        if (!$filename) {
            throw new \Exception('$filename is empty', 400);
        }

        // Throw if empty.
        if (!$updatedOn) {
            throw new \Exception('$updatedOn is empty', 400);
        }

        // Update Template(s).
        // https://medoo.in/api/update
        $result = $this->database->update('template', [
            'title' => $title,
            'slug' => $slug,
            'filename' => $slug,
            'updated_on' => $updatedOn
        ], [
            'uuid' => $uuid
        ]);

        // Throw if it fails.
        // Returns the number of rows affected by the last SQL statement.
        if ($result->rowCount() === 0) {
            throw new \Exception('Update row failed', 400);
        }

        // print_r($this->database->error());

        // Return the model if it is OK.
        return $template;
    }
}
