<?php
namespace Spectre\Template\Gateway\Insert;

use \Spectre\Core\Model\Model;
use \Spectre\Template\Gateway\Gateway;

class Template extends Gateway
{
    public function insert(Model $template)
    {
        // Get data from the model.
        $uuid = $template->getUuid();
        $slug = $template->getSlug();
        $title = $template->getTitle();
        $filename = $template->getFilename();
        $createdOn = $template->getCreatedOn();

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Throw if empty.
        if (!$slug) {
            throw new \Exception('$slug is empty', 400);
        }

        // Throw if empty.
        if (!$title) {
            throw new \Exception('$title is empty', 400);
        }

        // Throw if empty.
        if (!$filename) {
            throw new \Exception('$filename is empty', 400);
        }

        // Throw if empty.
        if (!$createdOn) {
            throw new \Exception('$createdOn is empty', 400);
        }

        // Insert template.
        // https://medoo.in/api/insert
        $result = $this->database->insert('template', [
            'uuid' => $uuid,
            'title' => $title,
            'slug' => $slug,
            'filename' => $filename,
            'created_on' => $createdOn,
            'updated_on' => $createdOn,
        ]);

        // Throw if it fails.
        // Returns the number of rows affected by the last SQL statement.
        if ($result->rowCount() === 0) {
            throw new \Exception('Insert row failed', 400);
        }

        // Return the model if it is OK.
        return $template;
    }
}
