<?php
namespace Spectre\Template\Gateway\Delete;

use \Spectre\Core\Model\Model;
use \Spectre\Template\Gateway\Gateway;

class Template extends Gateway
{
    public function delete(Model $template)
    {
        // Get data from the model.
        $uuid = $template->getUuid();

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Delete Template(s).
        // https://medoo.in/api/delete
        $result = $this->database->delete("template", [
            "uuid" => $uuid
        ]);

        // Check the number of rows affected by the last SQL statement.
        // Throw if it fails.
        if ($result->rowCount() === 0) {
            throw new \Exception('Delete row failed', 400);
        }

        // Return the model if it is OK.
        return $template;
    }
}
