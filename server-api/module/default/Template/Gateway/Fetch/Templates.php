<?php
namespace Spectre\Template\Gateway\Fetch;

use \Spectre\Template\Gateway\Gateway;

class Templates extends Gateway
{
    public function fetch($columns = [])
    {
        // Get template(s).
        // https://medoo.in/api/select
        $data = $this->database->select('template', $columns);

        // Return the result.
        return $data;
    }
}
