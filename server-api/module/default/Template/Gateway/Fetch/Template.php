<?php
namespace Spectre\Template\Gateway\Fetch;

use \Spectre\Template\Gateway\Gateway;

class Template extends Gateway
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($where) === 0) {
            throw new \Exception('$where is empty from the mapper', 400);
        }

        // Get Template.
        // https://medoo.in/api/get
        $data = $this->database->get('template', $columns, $where);

        // Throw error if no result found.
        if ($data === false) {
            throw new \Exception('No template found', 400);
        }

        // Return the result.
        return $data;
    }
}
