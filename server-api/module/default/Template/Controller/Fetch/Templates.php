<?php
namespace Spectre\Template\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Template\Controller\Controller;
use \Spectre\Template\Gateway\Fetch\Templates as FetchGateway;
use \Spectre\Template\Mapper\Fetch\Templates as FetchMapper;

class Templates extends Controller
{
    public function fetch(Request $request)
    {
        $gateway = new FetchGateway($this->database);
        $mapper = new FetchMapper($gateway);
        $collection = $mapper->fetch([
            'uuid',
            'slug',
            'title',
            'filename',
            'created_on',
            'updated_on',
        ]);

        $templates = $collection->getItems();
        foreach ($templates as $key => $template) {
            $template->setCreatedOn($this->timestamp->convert($template->getCreatedOn()));
            $template->setUpdatedOn($this->timestamp->convert($template->getUpdatedOn()));
        }

        return $collection->toArray();
    }
}
