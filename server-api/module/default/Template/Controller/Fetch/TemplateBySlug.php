<?php
namespace Spectre\Template\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Template\Controller\Controller;
use \Spectre\Template\Gateway\Fetch\Template as FetchGateway;
use \Spectre\Template\Mapper\Fetch\Template as FetchMapper;

class TemplateBySlug extends Controller
{
    public function fetch(Request $request, array $args)
    {
        $gateway = new FetchGateway($this->database);
        $mapper = new FetchMapper($gateway);
        $model = $mapper->fetch([
            'uuid',
            'slug',
            'title',
            'filename',
            'created_on',
            'updated_on',
        ], [
            'slug' => $args['slug'],
        ]);

        $model->setCreatedOn($this->timestamp->convert($model->getCreatedOn()));
        $model->setUpdatedOn($this->timestamp->convert($model->getUpdatedOn()));
        return $model->toArray();
    }
}
