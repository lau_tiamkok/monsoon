<?php
namespace Spectre\Template\Controller\Update;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Template\Controller\Controller;
use \Spectre\Template\Gateway\Update\Template as UpdateGateway;
use \Spectre\Template\Mapper\Update\Template as UpdateMapper;

class Template extends Controller
{
    public function update(Request $request)
    {
        // Get params and validate them here.
        $uuid = $request->getParam('uuid');
        $title = $request->getParam('title');
        $slug = $request->getParam('slug');
        $filename = $request->getParam('filename');

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Throw if empty.
        if (!$title) {
            throw new \Exception('$title is empty', 400);
        }

        // Throw if empty.
        if (!$slug) {
            throw new \Exception('$slug is empty', 400);
        }

        // Throw if empty.
        if (!$filename) {
            throw new \Exception('$filename is empty', 400);
        }

        // Create a timestamp.
        $date = new \DateTime();
        $timestamp = $date->getTimestamp();

        $gateway = new UpdateGateway($this->database);
        $mapper = new UpdateMapper($gateway);
        $model = $mapper->update([
            'uuid' => $uuid,
            'title' => $title,
            'slug' => $slug,
            'filename' => $filename,
            'updated_on' => $timestamp,
        ], [
            'uuid' => $uuid
        ]);

        return $model->toArray();
    }
}
