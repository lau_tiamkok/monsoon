<?php
namespace Spectre\Tag\Mapper\Fetch;

use \Spectre\Tag\Mapper\Mapper;

class Tags extends Mapper
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($columns) === 0) {
            throw new \Exception('$columns is empty from the controller', 400);
        }

        $collection = $this->gateway->fetch($columns, $where);
        return $this->mapCollection($collection);
    }
}
