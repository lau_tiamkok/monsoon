<?php
namespace Spectre\Tag\Mapper\Fetch;

use \Spectre\Tag\Mapper\Mapper;

class Tag extends Mapper
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($where) === 0) {
            throw new \Exception('$where is empty from the controller', 400);
        }

        $tag = $this->gateway->fetch($columns, $where);

        // Throw error if no tag found.
        if ($tag === null) {
            throw new \Exception('No tag found.', 400);
        }
        return $this->mapObject($tag);
    }
}
