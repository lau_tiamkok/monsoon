<?php
namespace Spectre\Tag\Mapper;

use \Spectre\Core\Strategy\GatewayInterface;
use \Spectre\Core\Mapper\Mapper as CoreMapper;

abstract class Mapper extends CoreMapper
{
    protected $gateway;
    protected $model = 'Spectre\Tag\Model\Tag';
    protected $collection = 'Spectre\Tag\Model\Tags';

    public function __construct(GatewayInterface $gateway)
    {
        $this->gateway = $gateway;
    }
}
