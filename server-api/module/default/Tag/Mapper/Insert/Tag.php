<?php
namespace Spectre\Tag\Mapper\Insert;

use \Spectre\Tag\Mapper\Mapper;

class Tag extends Mapper
{
    public function insert($params = [])
    {
        // Throw error if array is empty.
        if (count($params) === 0) {
            throw new \Exception('$params is empty from the controller', 400);
        }

        $model = $this->mapObject($params);
        return $this->gateway->insert($model);
    }
}
