<?php
namespace Spectre\Tag\Gateway\Update;

use \Spectre\Core\Model\Model;
use \Spectre\Tag\Gateway\Gateway;

class Tag extends Gateway
{
    public function update(Model $tag, array $where)
    {
        // Get data from the model.
        $title = $tag->getTitle();
        $slug = $tag->getSlug();
        $updatedOn = $tag->getUpdatedOn();

        // Get data from the params.
        $uuid = $where['uuid'];

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Throw if empty.
        if (!$slug) {
            throw new \Exception('$slug is empty', 400);
        }

        // Throw if empty.
        if (!$title) {
            throw new \Exception('$title is empty', 400);
        }

        // Throw if empty.
        if (!$updatedOn) {
            throw new \Exception('$updatedOn is empty', 400);
        }

        // Update Category(s).
        // https://medoo.in/api/update
        $result = $this->database->update('tag', [
            'slug' => $slug,
            'title' => $title,
            'updated_on' => $updatedOn
        ], [
            'uuid' => $uuid
        ]);

        // Throw if it fails.
        // Returns the number of rows affected by the last SQL statement.
        if ($result->rowCount() === 0) {
            throw new \Exception('Update row failed', 400);
        }

        // print_r($this->database->error());

        // Return the model if it is OK.
        return $tag;
    }
}
