<?php
namespace Spectre\Tag\Gateway\Delete;

use \Spectre\Core\Model\Model;
use \Spectre\Tag\Gateway\Gateway;

class Tag extends Gateway
{
    public function delete(Model $tag)
    {
        // Get data from the model.
        $uuid = $tag->getUuid();

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Delete Tag(s).
        // https://medoo.in/api/delete
        $result = $this->database->delete("tag", [
            "uuid" => $uuid
        ]);

        // Check the number of rows affected by the last SQL statement.
        // Throw if it fails.
        if ($result->rowCount() === 0) {
            throw new \Exception('Delete row failed', 400);
        }

        // Return the model if it is OK.
        return $tag;
    }
}
