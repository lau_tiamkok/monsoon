<?php
namespace Spectre\Tag\Gateway\Fetch;

use \Spectre\Tag\Gateway\Gateway;

class Tags extends Gateway
{
    public function fetch($columns = [], $where = [])
    {
        // Get tag(s).
        // https://medoo.in/api/select
        $data = $this->database->select('tag', $columns, $where);

        // Return the result.
        return $data;
    }
}
