<?php
namespace Spectre\Tag\Gateway\Fetch;

use \Spectre\Tag\Gateway\Gateway;

class Tag extends Gateway
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($where) === 0) {
            throw new \Exception('$where is empty from the mapper', 400);
        }

        // Get Tag.
        // https://medoo.in/api/get
        $data = $this->database->get('tag', $columns, $where);

        // Throw error if no result found.
        if (!$data) {
            throw new \Exception('No tag found', 400);
        }

        // Return the result.
        return $data;
    }
}
