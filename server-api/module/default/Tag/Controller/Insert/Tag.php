<?php
namespace Spectre\Tag\Controller\Insert;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// Uuid generator.
use \Ramsey\Uuid\Uuid;

use \Spectre\Tag\Controller\Controller;
use \Spectre\Tag\Gateway\Insert\Tag as InsertGateway;
use \Spectre\Tag\Mapper\Insert\Tag as InsertMapper;

class Tag extends Controller
{
    public function insert(Request $request)
    {
        // Get params and validate them here.
        // POST or PUT
        // $allPostPutVars = $request->getParsedBody();
        // foreach($allPostPutVars as $key => $param){
        //     //POST or PUT parameters list
        //     var_dump($param);
        // }
        // // Single POST/PUT parameter
        // $name = $allPostPutVars['name'];
        // $email = $allPostPutVars['email'];

        // Or, better:
        $title = $request->getParam('title'); // by key
        $slug = $request->getParam('slug'); // by key

        // Throw if empty.
        if (!$slug) {
            throw new \Exception('$slug is empty', 400);
        }

        // Throw if empty.
        if (!$title) {
            throw new \Exception('$title is empty', 400);
        }

        // Create a timestamp.
        $date = new \DateTime();
        $timestamp = $date->getTimestamp();
        // Or:
        // $timestamp = time();

        // Generate a version 3 (name-based and hashed with MD5) UUID object.
        // https://github.com/ramsey/uuid
        $uuid3 = Uuid::uuid3(Uuid::NAMESPACE_DNS, $title . $timestamp);
        $uuid = $uuid3->toString();

        $gateway = new InsertGateway($this->database);
        $mapper = new InsertMapper($gateway);
        $model = $mapper->insert([
            'uuid' => $uuid,
            'slug' => $slug,
            'title' => $title,
            'created_on' => $timestamp,

            // Below shows that you won't be able to set this in the model as it
            // is not declared in the model itself.
            'id' => 123
        ]);

        return $model->toArray();
    }
}
