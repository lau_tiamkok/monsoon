<?php
namespace Spectre\Tag\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Tag\Controller\Controller;
use \Spectre\Tag\Gateway\Fetch\Tags as FetchTagsGateway;
use \Spectre\Tag\Mapper\Fetch\Tags as FetchTagsMapper;

class TagsByType extends Controller
{
    public function fetch(Request $request, array $args)
    {
        $gateway = new FetchTagsGateway($this->database);
        $mapper = new FetchTagsMapper($gateway);
        $collection = $mapper->fetch([
            'uuid',
            'slug',
            'type',
            'title',
            'sort',
            'created_on',
            'updated_on',
        ], [
            'type' => $args['type']
        ]);

        $tags = $collection->getItems();
        foreach ($tags as $key => $tag) {
            $tag->setCreatedOn($this->timestamp->convert($tag->getCreatedOn()));
            $tag->setUpdatedOn($this->timestamp->convert($tag->getUpdatedOn()));
        }

        return $collection->toArray();
    }
}
