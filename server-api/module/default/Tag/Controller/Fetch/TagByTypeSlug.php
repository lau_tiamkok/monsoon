<?php
namespace Spectre\Tag\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Tag\Controller\Controller;
use \Spectre\Tag\Gateway\Fetch\Tag as FetchTagGateway;
use \Spectre\Tag\Mapper\Fetch\Tag as FetchTagMapper;

class TagByTypeSlug extends Controller
{
    public function fetch(Request $request, array $args)
    {
        $gateway = new FetchTagGateway($this->database);
        $mapper = new FetchTagMapper($gateway);
        $model = $mapper->fetch([
            'uuid',
            'slug',
            'type',
            'title',
            'sort',
            'created_on',
            'updated_on',
        ], [
            'slug' => $args['slug'],
            'type' => $args['type'],
        ]);

        $model->setCreatedOn($this->timestamp->convert($model->getCreatedOn()));
        $model->setUpdatedOn($this->timestamp->convert($model->getUpdatedOn()));
        return $model->toArray();
    }
}
