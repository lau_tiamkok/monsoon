<?php
namespace Spectre\Tag\Controller\Update;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Tag\Controller\Controller;
use \Spectre\Tag\Gateway\Update\Tag as UpdateGateway;
use \Spectre\Tag\Mapper\Update\Tag as UpdateMapper;

class Tag extends Controller
{
    public function update(Request $request)
    {
        // Get params and validate them here.
        $uuid = $request->getParam('uuid');
        $slug = $request->getParam('slug');
        $title = $request->getParam('title');

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Throw if empty.
        if (!$slug) {
            throw new \Exception('$slug is empty', 400);
        }

        // Throw if empty.
        if (!$title) {
            throw new \Exception('$title is empty', 400);
        }

        // Create a timestamp.
        $date = new \DateTime();
        $timestamp = $date->getTimestamp();

        $gateway = new UpdateGateway($this->database);
        $mapper = new UpdateMapper($gateway);
        $model = $mapper->update([
            'uuid' => $uuid,
            'slug' => $slug,
            'title' => $title,
            'updated_on' => $timestamp,
        ], [
            'uuid' => $uuid
        ]);

        return $model->toArray();
    }
}
