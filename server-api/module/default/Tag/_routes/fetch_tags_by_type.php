<?php
// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/tags/{type:[a-z-\-]+}', function (Request $request, Response $response, $args) {

    // If you want to access the Slim settings again.
    // $settings = $this->get("settings");

    // Autowiring the controller.
    $controller = $this->get('Spectre\Tag\Controller\Fetch\TagsByType');

    // Obtain result.
    $tags = $controller->fetch($request, $args);
    $response->getBody()->write(json_encode($tags));
});
