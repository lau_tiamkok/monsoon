<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/tags/{uuid:[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}}', function (Request $request, Response $response, $args) {

    // Autowiring the controller.
    $controller = $this->get('Spectre\Tag\Controller\Fetch\Tag');

    // Obtain result.
    $tag = $controller->fetch($request, $args);
    $response->getBody()->write(json_encode($tag));
});

// Notes:
// 1. Regex for UUID - https://stackoverflow.com/questions/136505/searching-for-uuids-in-text-with-regex
