<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/tags/{type:[a-z-\-]+}/{slug}', function (Request $request, Response $response, $args) {

    // Autowiring the controller.
    $controller = $this->get('Spectre\Tag\Controller\Fetch\TagByTypeSlug');

    // Obtain result.
    $tag = $controller->fetch($request, $args);
    $response->getBody()->write(json_encode($tag));
});
