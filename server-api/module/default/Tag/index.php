<?php
// Get location of current file.
$current = __DIR__;
$path = $current . '/_routes/';

// Scan and list the routes.
$routes = scandir($path);
$routes = array_diff(scandir($path), array('.', '..'));
foreach ($routes as $route) {
    require $path . $route;
}
