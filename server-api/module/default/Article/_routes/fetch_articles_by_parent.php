<?php
// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/{parent_slug}', function (Request $request, Response $response, $args) {

    // If you want to access the Slim settings again.
    // $settings = $this->get("settings");

    // Autowiring the controller.
    $controller = $this->get('Spectre\Article\Controller\Fetch\ArticlesByParent');

    // Obtain result.
    $articles = $controller->fetch($request, $args);
    $response->getBody()->write(json_encode($articles));
});
