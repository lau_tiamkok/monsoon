<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/{parent_slug}/{child_slug}', function (Request $request, Response $response, $args) {

    // Autowiring the controller.
    $controller = $this->get('Spectre\Article\Controller\Fetch\ArticleByParent');

    // Obtain result.
    $article = $controller->fetch($request, $args);
    $response->getBody()->write(json_encode($article));
});
