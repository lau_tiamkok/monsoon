<?php
namespace Spectre\Article\Model;

use \Spectre\Resource\Model\Model as Model;

class Resource extends Model
{
    protected $subtype;
    protected $title;
    protected $description;
    protected $caption;
    protected $href;
    protected $sort;

    /**
     * [setOptions description]
     * @param array $params [description]
     */
    public function setOptions(array $params)
    {
        foreach ($params as $key => $value) {
            switch ($key) {
                case 'subtype':
                    $this->setSubtype($value);
                    break;
                case 'title':
                    $this->setTitle($value);
                    break;
                case 'description':
                    $this->setDescription($value);
                    break;
                case 'caption':
                    $this->setCaption($value);
                    break;
                case 'href':
                    $this->setHref($value);
                    break;
                case 'sort':
                    $this->setSort($value);
                    break;
                case 'uuid':
                    $this->setUuid($value);
                    break;
                case 'user_uuid':
                    $this->setUserUuid($value);
                    break;
                case 'type':
                    $this->setType($value);
                    break;
                case 'source':
                    $this->setSource($value);
                    break;
                case 'ext':
                    $this->setExt($value);
                    break;
                case 'mime_type':
                    $this->setMimeType($value);
                    break;
                case 'created_on':
                    $this->setCreatedOn($value);
                    break;
                case 'updated_on':
                    $this->setUpdatedOn($value);
                    break;
            }
        }
    }

    // Setters:

    /**
     * [setTitle description]
     * @param [type] $type [description]
     */
    public function setSubtype($subtype)
    {
        $this->subtype = $subtype;

        return $this;
    }

    /**
     * [setTitle description]
     * @param [type] $title [description]
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * [setDescription description]
     * @param [type] $description [description]
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * [setcaption description]
     * @param [type] $caption [description]
     */
    public function setCaption($caption)
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * [setcaption description]
     * @param [type] $caption [description]
     */
    public function setHref($href)
    {
        $this->href = $href;

        return $this;
    }

    /**
     * [setSort description]
     * @param [type] $sort [description]
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    // Getters:

    /**
     * [getTitle description]
     * @return [type] [description]
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * [getTitle description]
     * @return [type] [description]
     */
    public function getSubtype()
    {
        return $this->subtype;
    }

    /**
     * [getTitle description]
     * @return [type] [description]
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * [getDescription description]
     * @return [type] [description]
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * [getcaption description]
     * @return [type] [description]
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * [getcaption description]
     * @return [type] [description]
     */
    public function getHref()
    {
        return $this->href;
    }

    /**
     * [getSort description]
     * @return [type] [description]
     */
    public function getSort()
    {
        return $this->sort;
    }
}
