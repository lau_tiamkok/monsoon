<?php
namespace Spectre\Article\Controller;

// Vendors.
use \Medoo\Medoo;
use \Monolog\Logger;

// Cores.
use \Spectre\Core\Controller\Controller as CoreController;
use \Spectre\Core\Model\Model;
use \Spectre\Core\Utils\TimestampConvertor;

// Modules/ locals.
use \Spectre\Article\Gateway\Fetch\ResourcesByArticle as FetchResourcesByArticleGateway;
use \Spectre\Article\Mapper\Fetch\ResourcesByArticle as FetchResourcesByArticleMapper;

abstract class Controller extends CoreController
{
    protected $database;
    protected $timestamp;

    public function __construct(
        Medoo $database,
        TimestampConvertor $timestamp,
        Logger $logger
    ) {
        $this->database = $database;
        $this->timestamp = $timestamp;

        // Log anything.
        $logger->addInfo("Logged from Spectre\Article\Controller\Controller");
    }

    public function fetchResources(Model $article, $subtype)
    {
        $gateway = new FetchResourcesByArticleGateway($this->database);
        $mapper = new FetchResourcesByArticleMapper($gateway);

        // Throw if empty.
        if (!$subtype) {
            throw new \Exception('$subtype is empty', 400);
        }

        // Columns to select.
        $columns = [
            'resource.uuid', // be specific to avoid MySQL ambiguous error.
            'user_uuid',
            'type',
            'source',
            'ext',
            'mime_type',
            'type',
            'subtype',
            'title',
            'description',
            'caption',
            'href',
            'sort',
            'created_on',
            'updated_on',
        ];

        // Where conditions.
        $where = [
            "article_uuid" => $article->getUuid(),
            "subtype" => $subtype,
        ];

        $collection = $mapper->fetch($columns, $where);

        $resources = $collection->getItems();
        foreach ($resources as $key => $resource) {
            $resource->setCreatedOn($this->timestamp->convert($resource->getCreatedOn()));
            $resource->setUpdatedOn($this->timestamp->convert($resource->getUpdatedOn()));
        }
        return $collection;
    }
}
