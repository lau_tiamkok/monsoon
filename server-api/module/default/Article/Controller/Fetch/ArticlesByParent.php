<?php
namespace Spectre\Article\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Article\Controller\Controller;
use \Spectre\Article\Gateway\Fetch\Article as FetchArticleGateway;
use \Spectre\Article\Gateway\Fetch\ArticlesByParent as FetchArticlesByParentGateway;
use \Spectre\Article\Mapper\Fetch\ArticlesByParent as FetchArticlesByParentMapper;

class ArticlesByParent extends Controller
{
    public function fetch(Request $request, array $args)
    {
        $gateway = new FetchArticleGateway($this->database);
        $gateway = new FetchArticlesByParentGateway($this->database, $gateway);
        $mapper = new FetchArticlesByParentMapper($gateway);
        $collection = $mapper->fetch([
            'uuid',
            'slug',
            'title',
            'content',
            'created_on',
            'updated_on',
        ], [
            'parent_slug' => $args['parent_slug']
        ]);

        // Inject resources here to simplify the process.
        $articles = $collection->getItems();
        foreach ($articles as $key => $article) {
            $article->setResources($this->fetchResources($article, 'carousal'));
            $article->setCreatedOn($this->timestamp->convert($article->getCreatedOn()));
            $article->setUpdatedOn($this->timestamp->convert($article->getUpdatedOn()));
        }

        return $collection->toArray();
    }
}
