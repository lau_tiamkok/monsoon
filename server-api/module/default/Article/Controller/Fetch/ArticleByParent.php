<?php
namespace Spectre\Article\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Article\Controller\Controller;
use \Spectre\Article\Gateway\Fetch\Article as FetchArticleGateway;
use \Spectre\Article\Gateway\Fetch\ArticleByParent as FetchArticleByParentGateway;
use \Spectre\Article\Mapper\Fetch\ArticleByParent as FetchArticleByParentMapper;

class ArticleByParent extends Controller
{
    public function fetch(Request $request, array $args)
    {
        $gateway = new FetchArticleGateway($this->database);
        $gateway = new FetchArticleByParentGateway($this->database, $gateway);
        $mapper = new FetchArticleByParentMapper($gateway);
        $model = $mapper->fetch([
            'uuid',
            'slug',
            'title',
            'content',
            'created_on',
            'updated_on',
        ], [
            "parent_slug" => $args['parent_slug'],
            "child_slug" => $args['child_slug'],
        ]);

        $model->setResources($this->fetchResources($model, 'carousal'));
        $model->setCreatedOn($this->timestamp->convert($model->getCreatedOn()));
        $model->setUpdatedOn($this->timestamp->convert($model->getUpdatedOn()));
        return $model->toArray();
    }
}
