<?php
namespace Spectre\Article\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Article\Controller\Controller;
use \Spectre\Article\Gateway\Fetch\Articles as FetchArticlesGateway;
use \Spectre\Article\Mapper\Fetch\Articles as FetchArticlesMapper;

class Articles extends Controller
{
    public function fetch(Request $request)
    {
        $gateway = new FetchArticlesGateway($this->database);
        $mapper = new FetchArticlesMapper($gateway);
        $collection = $mapper->fetch([
            'uuid',
            'slug',
            'title',
            'content',
            'created_on',
            'updated_on',
        ]);

        // Inject resources here to simplify the process.
        $articles = $collection->getItems();
        foreach ($articles as $key => $article) {
            $article->setResources($this->fetchResources($article, 'carousal'));
            $article->setCreatedOn($this->timestamp->convert($article->getCreatedOn()));
            $article->setUpdatedOn($this->timestamp->convert($article->getUpdatedOn()));
        }

        return $collection->toArray();
    }
}
