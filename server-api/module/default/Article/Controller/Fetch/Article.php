<?php
namespace Spectre\Article\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Article\Controller\Controller;
use \Spectre\Article\Gateway\Fetch\Article as FetchArticleGateway;
use \Spectre\Article\Mapper\Fetch\Article as FetchArticleMapper;

class Article extends Controller
{
    public function fetch(Request $request, array $args)
    {
        $gateway = new FetchArticleGateway($this->database);
        $mapper = new FetchArticleMapper($gateway);
        $model = $mapper->fetch([
            'uuid',
            'slug',
            'title',
            'content',
            'created_on',
            'updated_on',
        ], [
            'slug' => $args['slug'],
            'type' => 'page',
        ]);

        $model->setResources($this->fetchResources($model, 'carousal'));
        $model->setCreatedOn($this->timestamp->convert($model->getCreatedOn()));
        $model->setUpdatedOn($this->timestamp->convert($model->getUpdatedOn()));
        return $model->toArray();
    }
}
