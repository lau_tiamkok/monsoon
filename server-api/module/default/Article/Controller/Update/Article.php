<?php
namespace Spectre\Article\Controller\Update;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Article\Controller\Controller;
use \Spectre\Article\Gateway\Update\Article as UpdateGateway;
use \Spectre\Article\Mapper\Update\Article as UpdateMapper;

class Article extends Controller
{
    public function update(Request $request)
    {
        // Get params and validate them here.
        $uuid = $request->getParam('uuid');
        $title = $request->getParam('title');
        $content = $request->getParam('content');

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Throw if empty.
        if (!$title) {
            throw new \Exception('$title is empty', 400);
        }

        // Throw if empty.
        if (!$content) {
            throw new \Exception('$content is empty', 400);
        }

        // Create a timestamp.
        $date = new \DateTime();
        $timestamp = $date->getTimestamp();

        $gateway = new UpdateGateway($this->database);
        $mapper = new UpdateMapper($gateway);
        $model = $mapper->update([
            "uuid" => $uuid,
            "title" => $title,
            "content" => $content,
            'updated_on' => $timestamp,
        ], [
            "uuid" => $uuid
        ]);

        return $model->toArray();
    }
}
