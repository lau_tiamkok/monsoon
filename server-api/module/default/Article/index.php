<?php
// require '_routes/fetch_articles.php';
// require '_routes/fetch_article.php';
// require '_routes/fetch_articles_by_parent.php';
// require '_routes/fetch_article_by_parent.php';
// require '_routes/insert_article.php';
// require '_routes/update_article.php';
// require '_routes/delete_article.php';

// Get location of current file.
$path = __DIR__ . '/_routes/';

// Scan and list the routes.
$routes = scandir($path);
$routes = array_diff(scandir($path), array('.', '..'));
foreach ($routes as $route) {
    require $path . $route;
}
