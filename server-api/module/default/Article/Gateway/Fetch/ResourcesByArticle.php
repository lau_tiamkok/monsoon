<?php
namespace Spectre\Article\Gateway\Fetch;

use \Spectre\Article\Gateway\Gateway;

class ResourcesByArticle extends Gateway
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($where) === 0) {
            throw new \Exception('$where is empty from the controller', 400);
        }

        // Get article(s).
        // https://medoo.in/api/get
        $data = $this->database->select('article_resource', [
                '[>]resource' => [
                    'article_resource.resource_uuid' => 'uuid'
                ]
            ], $columns, [
                'article_resource.article_uuid' => $where['article_uuid'],
                'article_resource.subtype' => $where['subtype'],
            ]
        );

        // Return the result.
        return $data;
    }
}
