<?php
namespace Spectre\Article\Gateway\Fetch;

use \Spectre\Article\Gateway\Gateway;

class Article extends Gateway
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($where) === 0) {
            throw new \Exception('$where is empty from the mapper', 400);
        }

        // Get article.
        // https://medoo.in/api/get
        $data = $this->database->get('article', $columns, $where);

        // Throw error if no result found.
        if (!$data) {
            throw new \Exception('No article found', 400);
        }

        // Return the result.
        return $data;
    }
}
