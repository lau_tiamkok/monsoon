<?php
namespace Spectre\Article\Gateway\Fetch;

use \Spectre\Article\Gateway\ByParentGateway as Gateway;

class ArticlesByParent extends Gateway
{
    public function fetch($columns = [], $where = [])
    {
        // Get article.
        // https://medoo.in/api/get
        $parent = $this->fetchArticle->fetch($columns, [
            'slug' => $where['parent_slug']
        ]);

        // Get article(s).
        // https://medoo.in/api/select
        $data = $this->database->select('article_article',[
                '[>]article' => [
                    'article_article.article_uuid2' => 'uuid'
                ]
            ], $columns, [
                'article_article.article_uuid1' => $parent['uuid'],
                'LIMIT' => 6
            ]
        );

        // Return the result.
        return $data;
    }
}
