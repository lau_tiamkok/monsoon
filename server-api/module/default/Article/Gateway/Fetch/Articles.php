<?php
namespace Spectre\Article\Gateway\Fetch;

use \Spectre\Article\Gateway\Gateway;

class Articles extends Gateway
{
    public function fetch($columns = [])
    {
        // Get article(s).
        // https://medoo.in/api/select
        $data = $this->database->select('article', $columns);

        // Return the result.
        return $data;
    }
}
