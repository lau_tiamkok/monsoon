<?php
namespace Spectre\Article\Gateway\Fetch;

use \Spectre\Article\Gateway\ByParentGateway as Gateway;

class ArticleByParent extends Gateway
{
    public function fetch($columns = [], $where = [])
    {
        // Get article.
        // https://medoo.in/api/get
        $parent = $this->fetchArticle->fetch($columns, [
            'slug' => $where['parent_slug']
        ]);

        // Get article(s).
        // https://medoo.in/api/get
        $data = $this->database->get('article_article',[
                '[>]article' => [
                    'article_article.article_uuid2' => 'uuid'
                ]
            ], $columns, [
                'article_article.article_uuid1' => $parent['uuid'],
                'article.slug' => $where['child_slug'],
            ]
        );

        // Return the result.
        return $data;
    }
}
