<?php
namespace Spectre\Article\Gateway\Insert;

use \Spectre\Core\Model\Model;
use \Spectre\Article\Gateway\Gateway;

class Article extends Gateway
{
    public function insert(Model $article)
    {
        // Get data from the model.
        $uuid = $article->getUuid();
        $title = $article->getTitle();
        $content = $article->getContent();
        $createdOn = $article->getCreatedOn();

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Throw if empty.
        if (!$title) {
            throw new \Exception('$title is empty', 400);
        }

        // Throw if empty.
        if (!$content) {
            throw new \Exception('$content is empty', 400);
        }

        // Throw if empty.
        if (!$createdOn) {
            throw new \Exception('$createdOn is empty', 400);
        }

        // Insert article.
        // https://medoo.in/api/insert
        $result = $this->database->insert('article', [
            'uuid' => $uuid,
            'code' => $title, // to be continued...
            'public_code' => $title, // to be continued...
            'title' => $title,
            'content' => $content,
            'created_on' => $createdOn,
            'updated_on' => $createdOn,
        ]);

        // Throw if it fails.
        // Returns the number of rows affected by the last SQL statement.
        if ($result->rowCount() === 0) {
            throw new \Exception('Insert row failed', 400);
        }

        // Return the model if it is OK.
        return $article;
    }
}
