<?php
namespace Spectre\Article\Gateway\Update;

use \Spectre\Core\Model\Model;
use \Spectre\Article\Gateway\Gateway;

class Article extends Gateway
{
    public function update(Model $article, array $where)
    {
        // Get data from the model.
        $title = $article->getTitle();
        $content = $article->getContent();
        $updatedOn = $article->getUpdatedOn();

        // Get data from the params.
        $uuid = $where['uuid'];

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Throw if empty.
        if (!$title) {
            throw new \Exception('$title is empty', 400);
        }

        // Throw if empty.
        if (!$content) {
            throw new \Exception('$content is empty', 400);
        }

        // Throw if empty.
        if (!$updatedOn) {
            throw new \Exception('$updatedOn is empty', 400);
        }

        // Update article(s).
        // https://medoo.in/api/update
        $result = $this->database->update('article', [
            'title' => $title,
            'content' => $content,
            'updated_on' => $updatedOn
        ], [
            'uuid' => $uuid
        ]);

        // Throw if it fails.
        // Returns the number of rows affected by the last SQL statement.
        if ($result->rowCount() === 0) {
            throw new \Exception('Update row failed', 400);
        }

        // print_r($this->database->error());

        // Return the model if it is OK.
        return $article;
    }
}
