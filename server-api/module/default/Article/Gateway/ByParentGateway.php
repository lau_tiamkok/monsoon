<?php
namespace Spectre\Article\Gateway;

use \Spectre\Core\Gateway\Gateway as CoreGateway;
use \Medoo\Medoo;

abstract class ByParentGateway extends CoreGateway
{
    protected $database;
    protected $fetchArticle;

    public function __construct(
        Medoo $database,
        Gateway $fetchArticle
    ) {
        $this->database = $database;
        $this->fetchArticle = $fetchArticle;
    }
}
