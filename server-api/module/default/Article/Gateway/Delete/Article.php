<?php
namespace Spectre\Article\Gateway\Delete;

use \Spectre\Article\Gateway\Gateway;
use \Spectre\Article\Model\Model;

class Article extends Gateway
{
    public function delete(Model $article)
    {
        // Get data from the model.
        $uuid = $article->getUuid();

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Delete article(s).
        // https://medoo.in/api/delete
        $result = $this->database->delete("article", [
            "uuid" => $uuid
        ]);

        // Check the number of rows affected by the last SQL statement.
        // Throw if it fails.
        if ($result->rowCount() === 0) {
            throw new \Exception('Delete row failed', 400);
        }

        // Return the model if it is OK.
        return $article;
    }
}
