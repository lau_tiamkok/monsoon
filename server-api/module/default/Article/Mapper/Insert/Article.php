<?php
namespace Spectre\Article\Mapper\Insert;

use \Spectre\Article\Mapper\Mapper;

class Article extends Mapper
{
    public function insert($params = [])
    {
        // Throw error if array is empty.
        if (count($params) === 0) {
            throw new \Exception('$params is empty from the controller', 400);
        }

        $model = $this->mapObject($params);
        return $this->gateway->insert($model);
    }
}
