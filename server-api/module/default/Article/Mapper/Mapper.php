<?php
namespace Spectre\Article\Mapper;

use \Spectre\Core\Strategy\GatewayInterface;
use \Spectre\Core\Mapper\Mapper as CoreMapper;

abstract class Mapper extends CoreMapper
{
    protected $gateway;
    protected $model = 'Spectre\Article\Model\Model';
    protected $collection = 'Spectre\Article\Model\Collection';

    public function __construct(GatewayInterface $gateway)
    {
        $this->gateway = $gateway;
    }
}
