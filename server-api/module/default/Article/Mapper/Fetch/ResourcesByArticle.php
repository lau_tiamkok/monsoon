<?php
namespace Spectre\Article\Mapper\Fetch;

use \Spectre\Article\Mapper\Mapper;

class ResourcesByArticle extends Mapper
{
    public function fetch($columns = [], $where = [])
    {
        $this->model = 'Spectre\Article\Model\Resource';

        // Throw error if where search is not provide.
        if (count($columns) === 0) {
            throw new \Exception('$columns is empty from the controller', 400);
        }

        // Throw error if where search is not provide.
        if (count($where) === 0) {
            throw new \Exception('$where is empty from the controller', 400);
        }

        $collection = $this->gateway->fetch($columns, $where);
        return $this->mapCollection($collection);
    }
}
