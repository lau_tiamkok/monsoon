<?php
namespace Spectre\Article\Mapper\Fetch;

use \Spectre\Article\Mapper\Mapper;

class Articles extends Mapper
{
    public function fetch($columns = [])
    {
        // Throw error if where search is not provide.
        if (count($columns) === 0) {
            throw new \Exception('$columns is empty from the controller', 400);
        }

        $collection = $this->gateway->fetch($columns);
        return $this->mapCollection($collection);
    }
}
