<?php
namespace Spectre\Article\Mapper\Fetch;

use \Spectre\Article\Mapper\Mapper;

class ArticleByParent extends Mapper
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($columns) === 0) {
            throw new \Exception('$columns is empty from the controller', 400);
        }

        // Throw error if where search is not provide.
        if (count($where) === 0) {
            throw new \Exception('$where is empty from the controller', 400);
        }

        $article = $this->gateway->fetch($columns, $where);

        // Throw error if no article found.
        if ($article === null) {
            throw new \Exception('No article found.', 400);
        }

        return $this->mapObject($article);
    }
}
