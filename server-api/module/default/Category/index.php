<?php
// require '_routes/fetch_categories.php';
// require '_routes/fetch_category.php';
// require '_routes/fetch_categories_by_type.php';
// require '_routes/fetch_category_by_type.php';
// require '_routes/insert_category.php';
// require '_routes/update_category.php';
// require '_routes/delete_category.php';

// Get location of current file.
$current = __DIR__;
$path = $current . '/_routes/';

// Scan and list the routes.
$routes = scandir($path);
$routes = array_diff(scandir($path), array('.', '..'));
foreach ($routes as $route) {
    require $path . $route;
}
