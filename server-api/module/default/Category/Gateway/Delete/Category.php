<?php
namespace Spectre\Category\Gateway\Delete;

use \Spectre\Core\Model\Model;
use \Spectre\Category\Gateway\Gateway;

class Category extends Gateway
{
    public function delete(Model $category)
    {
        // Get data from the model.
        $uuid = $category->getUuid();

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Delete Category(s).
        // https://medoo.in/api/delete
        $result = $this->database->delete("category", [
            "uuid" => $uuid
        ]);

        // Check the number of rows affected by the last SQL statement.
        // Throw if it fails.
        if ($result->rowCount() === 0) {
            throw new \Exception('Delete row failed', 400);
        }

        // Return the model if it is OK.
        return $category;
    }
}
