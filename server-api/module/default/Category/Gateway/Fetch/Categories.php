<?php
namespace Spectre\Category\Gateway\Fetch;

use \Spectre\Category\Gateway\Gateway;

class Categories extends Gateway
{
    public function fetch($columns = [], $where = [])
    {
        // Get Category(s).
        // https://medoo.in/api/select
        $data = $this->database->select('category', $columns, $where);

        // Return the result.
        return $data;
    }
}
