<?php
namespace Spectre\Category\Gateway\Fetch;

use \Spectre\Category\Gateway\Gateway;

class Category extends Gateway
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($where) === 0) {
            throw new \Exception('$where is empty from the mapper', 400);
        }

        // Get category.
        // https://medoo.in/api/get
        $data = $this->database->get('category', $columns, $where);

        // Throw error if no result found.
        if (!$data) {
            throw new \Exception('No category found', 400);
        }

        // Return the result.
        return $data;
    }
}
