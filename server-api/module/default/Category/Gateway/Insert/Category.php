<?php
namespace Spectre\Category\Gateway\Insert;

use \Spectre\Core\Model\Model;
use \Spectre\Category\Gateway\Gateway;

class Category extends Gateway
{
    public function insert(Model $category)
    {
        // Get data from the model.
        $uuid = $category->getUuid();
        $slug = $category->getSlug();
        $title = $category->getTitle();
        $createdOn = $category->getCreatedOn();

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

         // Throw if empty.
        if (!$slug) {
            throw new \Exception('$slug is empty', 400);
        }

        // Throw if empty.
        if (!$title) {
            throw new \Exception('$title is empty', 400);
        }

        // Throw if empty.
        if (!$createdOn) {
            throw new \Exception('$createdOn is empty', 400);
        }

        // Insert Category.
        // https://medoo.in/api/insert
        $result = $this->database->insert('category', [
            'uuid' => $uuid,
            'slug' => $slug,
            // 'type' => $title, // to be continued...
            'title' => $title,
            'created_on' => $createdOn,
            'updated_on' => $createdOn,
        ]);

        // Throw if it fails.
        // Returns the number of rows affected by the last SQL statement.
        if ($result->rowCount() === 0) {
            throw new \Exception('Insert row failed', 400);
        }

        // Return the model if it is OK.
        return $category;
    }
}
