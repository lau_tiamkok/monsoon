<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/categories/{type}/{slug}', function (Request $request, Response $response, $args) {

    // Autowiring the controller.
    $controller = $this->get('Spectre\Category\Controller\Fetch\CategoryByTypeSlug');

    // Obtain result.
    $category = $controller->fetch($request, $args);
    $response->getBody()->write(json_encode($category));
});
