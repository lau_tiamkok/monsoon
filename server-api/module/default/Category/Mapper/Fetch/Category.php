<?php
namespace Spectre\Category\Mapper\Fetch;

use \Spectre\Category\Mapper\Mapper;

class Category extends Mapper
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($where) === 0) {
            throw new \Exception('$where is empty from the controller', 400);
        }

        $category = $this->gateway->fetch($columns, $where);

        // Throw error if no Category found.
        if ($category === null) {
            throw new \Exception('No Category found.', 400);
        }
        return $this->mapObject($category);
    }
}
