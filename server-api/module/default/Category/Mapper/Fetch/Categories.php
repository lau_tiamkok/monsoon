<?php
namespace Spectre\Category\Mapper\Fetch;

use \Spectre\Category\Mapper\Mapper;

class Categories extends Mapper
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($columns) === 0) {
            throw new \Exception('$columns is empty from the controller', 400);
        }

        $collection = $this->gateway->fetch($columns, $where);
        return $this->mapCollection($collection);
    }
}
