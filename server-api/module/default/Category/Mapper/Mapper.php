<?php
namespace Spectre\Category\Mapper;

use \Spectre\Core\Strategy\GatewayInterface;
use \Spectre\Core\Mapper\Mapper as CoreMapper;

abstract class Mapper extends CoreMapper
{
    protected $gateway;
    protected $model = 'Spectre\Category\Model\Category';
    protected $collection = 'Spectre\Category\Model\Categories';

    public function __construct(GatewayInterface $gateway)
    {
        $this->gateway = $gateway;
    }
}
