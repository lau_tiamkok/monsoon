<?php
namespace Spectre\Category\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Category\Controller\Controller;
use \Spectre\Category\Gateway\Fetch\Categories as FetchCategoriesGateway;
use \Spectre\Category\Mapper\Fetch\Categories as FetchCategoriesMapper;

class Categories extends Controller
{
    public function fetch(Request $request)
    {
        $gateway = new FetchCategoriesGateway($this->database);
        $mapper = new FetchCategoriesMapper($gateway);
        $collection = $mapper->fetch([
            'uuid',
            'slug',
            'type',
            'title',
            'sort',
            'created_on',
            'updated_on',
        ]);

        $categories = $collection->getItems();
        foreach ($categories as $key => $category) {
            $category->setCreatedOn($this->timestamp->convert($category->getCreatedOn()));
            $category->setUpdatedOn($this->timestamp->convert($category->getUpdatedOn()));
        }

        return $collection->toArray();
    }
}
