<?php
namespace Spectre\Category\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Category\Controller\Controller;
use \Spectre\Category\Gateway\Fetch\Category as FetchCategoryGateway;
use \Spectre\Category\Mapper\Fetch\Category as FetchCategoryMapper;

class Category extends Controller
{
    public function fetch(Request $request, array $args)
    {
        $gateway = new FetchCategoryGateway($this->database);
        $mapper = new FetchCategoryMapper($gateway);
        $model = $mapper->fetch([
            'uuid',
            'slug',
            'type',
            'title',
            'sort',
            'created_on',
            'updated_on',
        ], [
            'uuid' => $args['uuid'],
        ]);

        $model->setCreatedOn($this->timestamp->convert($model->getCreatedOn()));
        $model->setUpdatedOn($this->timestamp->convert($model->getUpdatedOn()));
        return $model->toArray();
    }
}
