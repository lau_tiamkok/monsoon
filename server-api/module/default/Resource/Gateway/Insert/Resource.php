<?php
namespace Spectre\Resource\Gateway\Insert;

use \Spectre\Resource\Gateway\Gateway;
use \Spectre\Resource\Model\Model;

class Resource extends Gateway
{
    public function insert(Model $resource)
    {
        // Get data from the model.
        $uuid = $resource->getUuid();
        $userUuid = $resource->getUserUuid();
        $type = $resource->getType();
        $source = $resource->getSource();
        $ext = $resource->getExt();
        $mimeType = $resource->getMimeType();
        $createdOn = $resource->getCreatedOn();

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Throw if empty.
        if (!$userUuid) {
            throw new \Exception('$userUuid is empty', 400);
        }

        // Throw if empty.
        if (!$type) {
            throw new \Exception('$type is empty', 400);
        }

        // Throw if empty.
        if (!$source) {
            throw new \Exception('$source is empty', 400);
        }

        // Throw if empty.
        if (!$ext) {
            throw new \Exception('$ext is empty', 400);
        }

        // Throw if empty.
        if (!$mimeType) {
            throw new \Exception('$mimeType is empty', 400);
        }

        // Throw if empty.
        if (!$createdOn) {
            throw new \Exception('$createdOn is empty', 400);
        }

        // Insert resource.
        // https://medoo.in/api/insert
        $result = $this->database->insert('resource', [
            'uuid' => $uuid,
            'user_uuid' => $userUuid,
            'type' => $type,
            'source' => $source,
            'ext' => $ext,
            'mime_type' => $mimeType,
            'created_on' => $createdOn
        ]);

        // Throw if it fails.
        // Returns the number of rows affected by the last SQL statement.
        if ($result->rowCount() === 0) {
            throw new \Exception('Insert row failed', 400);
        }

        // Return the model if it is OK.
        return $resource;
    }
}
