<?php
namespace Spectre\Resource\Gateway\Delete;

use \Spectre\Resource\Gateway\Gateway;
use \Spectre\Resource\Model\Model;

class Resource extends Gateway
{
    public function delete(Model $resource)
    {
        // Get data from the model.
        $uuid = $resource->getUuid();

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        // Delete resource(s).
        // https://medoo.in/api/delete
        $result = $this->database->delete("resource", [
            "uuid" => $uuid
        ]);

        // Check the number of rows affected by the last SQL statement.
        // Throw if it fails.
        if ($result->rowCount() === 0) {
            throw new \Exception('Delete row failed', 400);
        }

        // Return the model if it is OK.
        return $resource;
    }
}
