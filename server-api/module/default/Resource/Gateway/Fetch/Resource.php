<?php
namespace Spectre\Resource\Gateway\Fetch;

use \Spectre\Resource\Gateway\Gateway;

class Resource extends Gateway
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($where) === 0) {
            throw new \Exception('$where is empty from the mapper', 400);
        }

        // Get resource.
        // https://medoo.in/api/get
        $data = $this->database->get('resource', $columns, $where);

        // Throw error if no result found.
        if ($data === false) {
            throw new \Exception('No resource found', 400);
        }

        // Return the result.
        return $data;
    }
}
