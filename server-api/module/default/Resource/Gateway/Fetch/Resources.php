<?php
namespace Spectre\Resource\Gateway\Fetch;

use \Spectre\Resource\Gateway\Gateway;

class Resources extends Gateway
{
    public function fetch($columns = [])
    {
        // Get resource(s).
        // https://medoo.in/api/select
        $data = $this->database->select('resource', $columns);

        // Return the result.
        return $data;
    }
}
