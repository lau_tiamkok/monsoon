<?php
// require '_routes/fetch_resources.php';
// require '_routes/fetch_resource.php';
// require '_routes/insert_resource.php';
// require '_routes/delete_resource.php';

// Get location of current file.
$path = __DIR__ . '/_routes/';

// Scan and list the routes.
$routes = scandir($path);
$routes = array_diff(scandir($path), array('.', '..'));
foreach ($routes as $route) {
    require $path . $route;
}
