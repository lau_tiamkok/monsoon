<?php
namespace Spectre\Resource\Model;

use \Spectre\Core\Model\Model as AbstractModel;

class Model extends AbstractModel
{
    protected $uuid;
    protected $user_uuid;
    protected $type;
    protected $source;
    protected $ext;
    protected $mime_type;
    protected $created_on;
    protected $updated_on;

    /**
     * [__construct description]
     * @param array $params [description]
     */
    public function __construct(array $params = [])
    {
        $this->setOptions($params);
    }

    /**
     * [setOptions description]
     * @param array $params [description]
     */
    public function setOptions(array $params)
    {
        foreach ($params as $key => $value) {
            switch ($key) {
                case 'uuid':
                    $this->setUuid($value);
                    break;
                case 'user_uuid':
                    $this->setUserUuid($value);
                    break;
                case 'type':
                    $this->setType($value);
                    break;
                case 'source':
                    $this->setSource($value);
                    break;
                case 'ext':
                    $this->setExt($value);
                    break;
                case 'mime_type':
                    $this->setMimeType($value);
                    break;
                case 'created_on':
                    $this->setCreatedOn($value);
                    break;
                case 'updated_on':
                    $this->setUpdatedOn($value);
                    break;
            }
        }
    }

    // Setters:

    /**
     * [setUuid description]
     * @param [type] $uuid [description]
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * [setType description]
     * @param [type] $type [description]
     */
    public function setUserUuid($user_uuid)
    {
        $this->user_uuid = $user_uuid;
        return $this;
    }

    /**
     * [setType description]
     * @param [type] $type [description]
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * [setSource description]
     * @param [type] $source [description]
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }

    /**
     * [setExt description]
     * @param [type] $ext [description]
     */
    public function setExt($ext)
    {
        $this->ext = $ext;
        return $this;
    }

    /**
     * [setMimeType description]
     * @param [type] $mime_type [description]
     */
    public function setMimeType($mime_type)
    {
        $this->mime_type = $mime_type;
        return $this;
    }

    /**
     * [setCreatedOn description]
     * @param [type] $createdOn [description]
     */
    public function setCreatedOn($createdOn)
    {
        $this->created_on = $createdOn;
        return $this;
    }

    /**
     * [setUpdatedOn description]
     * @param [type] $updatedOn [description]
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updated_on = $updatedOn;
        return $this;
    }

    // Getters:

    /**
     * [getUuid description]
     * @return [type] [description]
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * [getType description]
     * @return [type] [description]
     */
    public function getUserUuid()
    {
        return $this->user_uuid;
    }

    /**
     * [getType description]
     * @return [type] [description]
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * [getSource description]
     * @return [type] [description]
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * [getExt description]
     * @return [type] [description]
     */
    public function getExt()
    {
        return $this->ext;
    }

    /**
     * [getMimeType description]
     * @return [type] [description]
     */
    public function getMimeType()
    {
        return $this->mime_type;
    }

    /**
     * [getCreatedOn description]
     * @return [type] [description]
     */
    public function getCreatedOn()
    {
        return $this->created_on;
    }

    /**
     * [getUpdatedOn description]
     * @return [type] [description]
     */
    public function getUpdatedOn()
    {
        return $this->updated_on;
    }
}
