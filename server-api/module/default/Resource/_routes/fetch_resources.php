<?php
// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/resources', function (Request $request, Response $response, $args) {

    // If you want to access the Slim settings again.
    // $settings = $this->get("settings");

    // Log anything.
    // $this->get('Monolog\Logger')->addInfo("Logged from route /resources");

    // Autowiring the controller.
    $controller = $this->get('Spectre\Resource\Controller\Fetch\Resources');

    // Obtain result.
    $resources = $controller->fetch($request);
    $response->getBody()->write(json_encode($resources));
});
