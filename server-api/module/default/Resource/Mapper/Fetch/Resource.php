<?php
namespace Spectre\Resource\Mapper\Fetch;

use \Spectre\Resource\Mapper\Mapper;

class Resource extends Mapper
{
    public function fetch($columns = [], $where = [])
    {
        // Throw error if where search is not provide.
        if (count($where) === 0) {
            throw new \Exception('$where is empty from the controller', 400);
        }

        $resource = $this->gateway->fetch($columns, $where);

        // Throw error if no resource found.
        if ($resource === null) {
            throw new \Exception('No resource found.', 400);
        }
        return $this->mapObject($resource);
    }
}
