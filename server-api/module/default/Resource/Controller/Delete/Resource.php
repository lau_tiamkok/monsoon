<?php
namespace Spectre\Resource\Controller\Delete;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Resource\Controller\Controller;
use \Spectre\Resource\Gateway\Delete\Resource as DeleteGateway;
use \Spectre\Resource\Mapper\Delete\Resource as DeleteMapper;

class Resource extends Controller
{
    public function delete(Request $request)
    {
        // Get params and validate them here.
        $uuid = $request->getParam('uuid');

        // Throw if empty.
        if (!$uuid) {
            throw new \Exception('$uuid is empty', 400);
        }

        $gateway = new DeleteGateway($this->database);
        $mapper = new DeleteMapper($gateway);
        $model = $mapper->delete([
            "uuid" => $uuid
        ]);

        return $model->toArray();
    }
}
