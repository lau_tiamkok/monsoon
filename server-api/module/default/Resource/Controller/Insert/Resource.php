<?php
namespace Spectre\Resource\Controller\Insert;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// Uuid generator.
use \Ramsey\Uuid\Uuid;

use \Spectre\Resource\Controller\Controller;
use \Spectre\Resource\Gateway\Insert\Resource as InsertGateway;
use \Spectre\Resource\Mapper\Insert\Resource as InsertMapper;

class Resource extends Controller
{
    public function insert(Request $request)
    {
        // Get params and validate them here.
        // POST or PUT
        // $allPostPutVars = $request->getParsedBody();
        // foreach($allPostPutVars as $key => $param){
        //     //POST or PUT parameters list
        //     var_dump($param);
        // }
        // // Single POST/PUT parameter
        // $userUuid = $allPostPutVars['user_uuid'];
        // $source = $allPostPutVars['source'];

        // Or, better:
        $userUuid = $request->getParam('user_uuid');
        $type = $request->getParam('type');
        $source = $request->getParam('source');
        $ext = $request->getParam('ext');
        $mimeType = $request->getParam('mime_type');

        // Throw if empty.
        if (!$userUuid) {
            throw new \Exception('$userUuid is empty', 400);
        }

        // Throw if empty.
        if (!$type) {
            throw new \Exception('$type is empty', 400);
        }

        // Throw if empty.
        if (!$source) {
            throw new \Exception('$source is empty', 400);
        }

        // Throw if empty.
        if (!$ext) {
            throw new \Exception('$ext is empty', 400);
        }

        // Throw if empty.
        if (!$mimeType) {
            throw new \Exception('$mimeType is empty', 400);
        }

        // Create a timestamp.
        $date = new \DateTime();
        $timestamp = $date->getTimestamp();
        // Or:
        // $timestamp = time();

        // Generate a version 3 (user_uuid-based and hashed with MD5) UUID object.
        // https://github.com/ramsey/uuid
        $uuid3 = Uuid::uuid3(Uuid::NAMESPACE_DNS, $source . $timestamp);
        $uuid = $uuid3->toString();

        $gateway = new InsertGateway($this->database);
        $mapper = new InsertMapper($gateway);
        $model = $mapper->insert([
            'uuid' => $uuid,
            'user_uuid' => $userUuid,
            'type' => $type,
            'source' => $source,
            'ext' => $ext,
            'mime_type' => $mimeType,
            'created_on' => $timestamp,
            'updated_on' => $timestamp,

            // Below shows that you won't be able to set this in the model as it
            // is not declared in the model itself.
            'id' => 123
        ]);

        return $model->toArray();
    }
}
