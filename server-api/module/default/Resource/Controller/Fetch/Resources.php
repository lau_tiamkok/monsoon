<?php
namespace Spectre\Resource\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Resource\Controller\Controller;
use \Spectre\Resource\Gateway\Fetch\Resources as FetchGateway;
use \Spectre\Resource\Mapper\Fetch\Resources as FetchMapper;

class Resources extends Controller
{
    public function fetch(Request $request)
    {
        $gateway = new FetchGateway($this->database);
        $mapper = new FetchMapper($gateway);
        $collection = $mapper->fetch([
            'uuid',
            'user_uuid',
            'type',
            'source',
            'ext',
            'mime_type',
            'created_on',
            'updated_on',
        ]);

        $resources = $collection->getItems();
        foreach ($resources as $key => $resource) {
            $resource->setCreatedOn($this->timestamp->convert($resource->getCreatedOn()));
            $resource->setUpdatedOn($this->timestamp->convert($resource->getUpdatedOn()));
        }

        return $collection->toArray();
    }
}
