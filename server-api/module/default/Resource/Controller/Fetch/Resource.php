<?php
namespace Spectre\Resource\Controller\Fetch;

// PSR 7 standard.
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Spectre\Resource\Controller\Controller;
use \Spectre\Resource\Gateway\Fetch\Resource as FetchGateway;
use \Spectre\Resource\Mapper\Fetch\Resource as FetchMapper;

class Resource extends Controller
{
    public function fetch(Request $request, array $args)
    {
        $gateway = new FetchGateway($this->database);
        $mapper = new FetchMapper($gateway);
        $model = $mapper->fetch([
            'uuid',
            'user_uuid',
            'type',
            'source',
            'ext',
            'mime_type',
            'created_on',
            'updated_on',
        ], [
            "uuid" => $args['uuid']
        ]);

        $model->setCreatedOn($this->timestamp->convert($model->getCreatedOn()));
        $model->setUpdatedOn($this->timestamp->convert($model->getUpdatedOn()));
        return $model->toArray();
    }
}
