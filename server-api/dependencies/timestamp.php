<?php
// Tell the container how to construct TimestampVisitor.
$container->add('Spectre\Core\Utils\TimestampConvertor', function() use ($container) {
    $settings = $container->get("settings");
    $convertor = new \Spectre\Core\Utils\TimestampConvertor();
    $convertor->setFormat($settings['date_format']);
    $convertor->setTimezone($settings['timezone']);
    return $convertor;
});
