<?php
// Add or modify modules to the list here.
return [
    'default/Nav/',
    'default/Setting/',
    'default/User/',
    'default/Category/',
    'default/Tag/',
    'default/Resource/',
    'default/Template/',
    'default/Article/',
];
