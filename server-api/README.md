# Requirements

1. PHP 7.0.x at least.
2. MySQL
3. Apache
4. Slim Framework or any other micro-framework

# Installation

## Dependencies And Autoloader

1. Make sure you have Composer installed in your local machine.

2. Navigate to your project directory and run the line below to fetch all the dependencies and compiling PSR-4 autoloader:

    `$ composer update`

## Database Tables

1. Get the latest version of Website EER Diagram in bin/eer-diagram/. Open it with MySQL Workbench, export it and install the tables with phpMyAdmin.

2. Make sure you change the database details in /your/project/path/config/database.php

## API Endpoints

1. Make sure you have the **mod-rewite** enabled.

2. Run the app with PHP's built-in webserver:

    $ cd [my-app-name]
    $ php -S 0.0.0.0:8080 -t public public/index.php

3. Then access your app at http://127.0.1.1:8080

    Below are some default available endpoints

    * http://127.0.1.1:8080/api/home
    * http://127.0.1.1:8080/api/about
    * http://127.0.1.1:8080/api/contact
    * http://127.0.1.1:8080/api/blog
    * http://127.0.1.1:8080/api/blog/sample-blog-article-1
    * http://127.0.1.1:8080/api/blog/sample-blog-article-2
    * http://127.0.1.1:8080/api/blog/sample-blog-article-3

# Development

You should always develop your project in `public/`, `config/`, and `module/`.

## Structure

All PHP files aside from the front controller are placed **outside of the publicly accessible folder**, so they can't be accessed regardless of the status of the server:

```
/path/to/project/
    bootstrap.php
    config/
    library/
    module/
    source/
    vendor/
    public/
      .htaccess
      index.php
```

Module examples:

```
module/
    home/
        controllers/
        models/
        views/
    blog/
        controllers/
        models/
        views/
    news/
        controllers/
        models/
        views/
```

For instance the `User` module should have the following directories and similar files:

```
_routes/
    insert_user.php
    update_user.php
    fetch_users.php
    fetch_user.php
    delete_user.php

Controller/
    Controller.php <-- abstract class to be extended by the following classes:
    InsertController.php
    UpdateController.php
    FetchController.php
    DeleteController.php

Gateway/
    Gateway.php <-- abstract class to be extended by the following classes:
    InsertGateway.php
    UpdateGateway.php
    FetchGateway.php
    DeleteGateway.php

Mapper/
    Mapper.php <-- abstract class to be extended by the following classes:
    InsertMapper.php
    UpdateMapper.php
    FetchMapper.php
    DeleteMapper.php

Model/
    Collection.php
    Model.php

index.php <-- the user main route.
```

# Best Practice

## Apache vs PHP's built-in webserver

If you don't want to use PHP's built-in webserver, you can use Apache that you already installed.

1. As all PHP files aside from the front controller are placed **outside of the publicly accessible folder**, the `public/` directory is the only directory that must be published to the web, so we can write the following in our virtual host file.

```
DocumentRoot /path/to/project/public
<Directory "/path/to/project/public">
  # other setting here
</Directory>
```

2. The .htaccess file then redirects all non-existing URLs to the front controller index.php:

```
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^ index.php [QSA,L]
```

3.  After configuring the virtual host file above, then you can access the app without `your-project-name/` and `public/` in your URL:

`http://localhost`

## PSR-2 & PSR-4

This app follows PSR-2 coding standard and PSR-4 for autoloading. Please check out the links below for more information.

    * http://www.php-fig.org/psr/psr-2/
    * http://www.php-fig.org/psr/psr-4/

To avoid using the same name for two classes, take the full advantage of namespace in PHP.

Referring to http://www.php-fig.org/psr/psr-1/

```
// PHP 5.3 and later:
namespace Vendor\Model;

class Foo
{
}

// PHP 5.2.x and earlier:
class Vendor_Model_Foo
{
}

```

## Naming Convention

1. The names for directories and files are written in long form, you may find them in short form in some directories and files -

    * `dist` for `distribution`
    * `nav`  for `navigation`
    * `confiq`  for `configuration`

    Try **not** to abbreviate names so other programmers can understand the meaning of the names without deciphering them.

2. For a complicated program, you should try to name your directories, files, classes, functions, variables, etc explicitly - in long forms to avoid too much abbreviations. For instance, `$op` is harder to decipher, but `$object_page` or `$objectPage` is easier for maintenance for other programmers.

3. **Avoid** naming your classes using any of these keywords below **alone** because they are PHP reserved keywords,

    * default
    * global
    * class
    * trait

    For more PHP reserved keywords and predefined classes, please go to http://php.net/manual/en/reserved.php

4. The naming convention in this application,

    |   Classes   |  Methods   |  Properties  | Functions  | Variables  |
    |-------------|------------|--------------|------------|------------|
    | PascalCase  | camelCase  | camelCase    | camelCase  | camelCase  |


5. There are **no rules** for naming files, except when you use a framework. You can feel free for naming your files just as you want, however, if you are using a framework, it can cause an error when trying to load the files. The best implementation is the one you feel best when using.

PHP makes no difference between any class or interface or abstract class. The autoloader function you define always gets the name of the thing to autoload, and no kind of hint which one it was.

Names of files should not have any suffixes of prefixes - only name of class, to make it possible find them by autoloader.

In the end you just use your classes by their name. So find good names instead of technical names is probably the bottom line.

`MyClass.class` is a **bad idea** that could allow someone to view your source code if they request that file by name. Using the `.php` extension ensures that the the user will only see the output after the file is processed by the PHP interpreter which for a file that contains nothing but a class is a blank page.

Below are references for naming conventions,

* https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md
* http://framework.zend.com/manual/1.12/en/coding-standard.naming-conventions.html
* http://pear.php.net/manual/en/standards.php

Naming convention of popular frameworks,

|      PHP Project      |   Classes   |  Methods   |  Properties  | Functions  | Variables  |
|-----------------------|-------------|------------|--------------|------------|------------|
| Akelos Framework      | PascalCase  | camelCase  | camelCase    | lower_case | lower_case |
| CakePHP Framework     | PascalCase  | camelCase  | camelCase    | camelCase  | camelCase  |
| CodeIgniter Framework | Proper_Case | lower_case | lower_case   | lower_case | lower_case |
| Concrete5 CMS         | PascalCase  | camelCase  | camelCase    | lower_case | lower_case |
| Doctrine ORM          | PascalCase  | camelCase  | camelCase    | camelCase  | camelCase  |
| Drupal CMS            | PascalCase  | camelCase  | camelCase    | lower_case | lower_case |
| Joomla CMS            | PascalCase  | camelCase  | camelCase    | camelCase  | camelCase  |
| modx CMS              | PascalCase  | camelCase  | camelCase    | camelCase  | lower_case |
| Pear Framework        | PascalCase  | camelCase  | camelCase    |            |            |
| Prado Framework       | PascalCase  | camelCase  | Pascal/camel |            | lower_case |
| SimplePie RSS         | PascalCase  | lower_case | lower_case   | lower_case | lower_case |
| Symfony Framework     | PascalCase  | camelCase  | camelCase    | camelCase  | camelCase  |
| WordPress CMS         |             |            |              | lower_case | lower_case |
| Zend Framework        | PascalCase  | camelCase  | camelCase    | camelCase  | camelCase  |
