An extended project of [Spectre - a framework-agnostic web application](https://github.com/lautiamkok/spectre).

Quick start
=============

## Server API / PHP

``` bash
# Dependencies
$ composer update

# Production build
$ php -S 0.0.0.0:8181 -t public
```

Access it at http://localhost:8181/

## Server Client / Nuxt

``` bash
# Dependencies
$ npm install

# Development
$ npm run dev

# Production build
$ npm start
```

Access it at http://localhost:3000/

Notes
=============

1. You must run these two apps concurrently.

References
=============

* https://nuxtjs.org/guide/commands/
